﻿using System;
using System.Runtime.Serialization;

namespace ConsoleApp2
{
    [Serializable]
    internal class FoodItemNotFound : Exception
    {
        public FoodItemNotFound()
        {
        }

        public FoodItemNotFound(string message) : base(message)
        {
        }

        public FoodItemNotFound(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FoodItemNotFound(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}