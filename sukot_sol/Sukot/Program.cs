﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            FoodCatalog kfc = new FoodCatalog();
            //kfc.Add(new FoodItem("chicken wings", 70, true, 2.5));
            //kfc.Add(new FoodItem("sweet potato", 80, true, 2.5));
            //kfc.Add(new FoodItem("hot beef",   80.5, true, 2.5));
            //kfc.Add(new FoodItem("many vegg",   80, true, 2.5));

            // 79.5: 80a 90b 79c 77d --> a, c
            kfc.Add(new FoodItem("chicken wings", 80, true, 2.5));
            kfc.Add(new FoodItem("sweet potato", 90, true, 2.5));
            kfc.Add(new FoodItem("hot beef", 79, true, 2.5));
            kfc.Add(new FoodItem("many vegg", 77, true, 2.5));

            List<FoodItem> items = kfc[79.5];
        }
    }
}
