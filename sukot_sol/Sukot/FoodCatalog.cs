﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class CompareFoodItemByDiff : IComparer<FoodItem>
    {
        public double m_search_for;
        public CompareFoodItemByDiff(double search_for)
        {
            m_search_for = search_for;
        }
        public int Compare(FoodItem x, FoodItem y)
        {
            // who will be first ?
            // x or y ?
            // if x is close to the number i am looking for --> x will be first
            // if y is close to the number i am looking for --> y will be first
            // if x and y close the same return 0
            double diffx = Math.Abs(x.Price - m_search_for);
            double diffy = Math.Abs(y.Price - m_search_for);
            if (diffx == diffy)
                return 0;
            if (diffx > diffy)
                return -1;
            return 1;

        }
    }
    class FoodCatalog
    {
        private List<FoodItem> m_food_items = new List<FoodItem>();
        public void Add(FoodItem fi)
        {
            m_food_items.Add(fi);
        }
        public int Count
        {
            get
            {
                return m_food_items.Count;
            }
        }
        public int EatCount
        {
            get

            {
                List<FoodItem> results = new List<FoodItem>();
                foreach (FoodItem food in m_food_items)
                {
                    if (food.IsFood)
                        results.Add(food);
                }
                return results.Count;

                //List<FoodItem> results_list = m_food_items.Where(food => food.IsFood == true).ToList();
                //return m_food_items.Where(food => food.IsFood == true).Count();
            }
        }
        public int DrinkCount
        {
            get
            {
                return m_food_items.Where(food => food.IsFood == false).Count();
            }
        }

        public FoodItem this[string food_name]
        {
            get
            {
                //FoodItem result_item = null;
                //foreach (FoodItem food in m_food_items)
                //{
                //    if (food.Name == food_name)
                //    {
                //        result_item = food;
                //        break;
                //    }
                //}
                
                FoodItem result = m_food_items.FirstOrDefault(food => food.Name == food_name);
                if (result == null)
                    throw new FoodItemNotFound($"cannot find {food_name}");
                return result;

            }
        }

        public List<FoodItem> this[double price]
        {
            get
            {

                // 1 - simple but ugly -- fast
                // List : 80
                //        10
                //        search 80
                //        min : 0
                //        current : 1
                //        80, 80, 81, 70
                //        79, 80, 77, 90
                // 80:   70a 80b 81c 80d --> b, d
                // 79.5: 80a 90b 79c 77d --> a, c
                // 79.5: 79.5a 90b 79c 77d --> a
                // 79.5: 79a 79.7b 80c 82d --> b
                // sort -- n log(n) -- 100 * 2 -- 200
                double diff_min = Math.Abs(price - m_food_items[0].Price);
                List<FoodItem> result = new List<FoodItem>();
                result.Add(m_food_items[0]);

                for (int i = 1; i < m_food_items.Count; i++)
                {
                    double local_diff = Math.Abs(price - m_food_items[i].Price);
                    if (local_diff < diff_min)
                    {
                        result.Clear();
                        result.Add(m_food_items[i]);
                        diff_min = local_diff;
                    }
                    else if (local_diff == diff_min)
                    {
                        result.Add(m_food_items[i]);
                    }
                }
                //return result;

                // 67
                // key : 40 , value : list .....
                // key : 80 , value : list .....
                // key : 95 , value : list .....



                // 2 - slower but magniv!
                CompareFoodItemByDiff compareFoodItemByDiff = new CompareFoodItemByDiff(price);
                m_food_items.Sort(compareFoodItemByDiff);
                List<FoodItem> results = new List<FoodItem>();
                double diff = Math.Abs(m_food_items[0].Price - price);
                int index = 0;
                while (diff == Math.Abs(m_food_items[index].Price - price))
                {
                    results.Add(m_food_items[index]);
                }
                return result;
            }
        }
    }
}
