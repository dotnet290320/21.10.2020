﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class FoodItem
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public bool IsFood { get; set; }
        public double Volume { get; set; }

        public FoodItem(string name, double price, bool isFood, double volume)
        {
            //if (name== null || name=="") -- if(string.IsNullOrEmpty(name)))
            if (string.IsNullOrEmpty(name) || name.Length < 5 || price == 0 || volume == 0)
                throw new ArgumentException("wrong input .....");
            if (price < 0)
                throw new NegativePriceException("...");
            Name = name;
            Price = price;
            IsFood = isFood;
            Volume = volume;
        }

        public static bool operator ==(FoodItem f1, FoodItem f2)
        {
            if (f1 == null && f2 == null)
                return true;
            if (f1 == null || f2 == null)
                return false;

            // Sushi sushi ==> SUSHI SUSHI
            return f1.Name.ToUpper() == f2.Name.ToUpper();
        }
        public static bool operator !=(FoodItem f1, FoodItem f2)
        {
            return !(f1 == f2);
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Name} {Price}";
        }
    }
}
