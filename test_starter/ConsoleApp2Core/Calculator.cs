﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2Core
{

    // TDD
    // E2E test 
    // intergation test
    // Unit testing
    public class Calculator
    {
        // 5 + 3 = 8.0
        public double Add(double x, double y)
        {
            return x + y;
        }

        // x = 9 , y = 3 , expected = 3.0
        // x = 10 y = 6, expected = 1.66666
        public double Divide(double x, double y)
        {
            if (y == 0)
                throw new DivideByZeroException($"Tried to divide {x}/{y}. but y is zero!!");
            return x / y;
        }
    }
}
