using ConsoleApp2Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConsoleApp2CoreTest2
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAdd()
        {
            Calculator calc = new Calculator();
            double result = calc.Add(3.1, 4.7);
            Assert.IsTrue(result.ToString().StartsWith("7.8"));
            // Assert.IsTrue(result == 7.8);
            //Assert.AreSame(result, 7.8);
        }

        [TestMethod]
        public void TestDivide()
        {
            Calculator calc = new Calculator();
            double result = calc.Divide(4.0, 2.0);
            Assert.IsTrue((result).Equals(2.0));
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        // this is what needed for expecting an exception in test
        public void TestDivideExpectedDivideByZeroException()
        {
            Calculator calc = new Calculator();
            double result = calc.Divide(4.0, 0);
        }
    }
}
